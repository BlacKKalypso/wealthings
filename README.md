Android Application sous API 26
===================

Installation
------------


- Installez la dernière version d'[Android Studio][1]

Clonez le dépot et importez sur **Android Studio**
```bash
$ git clone git@gitlab.com:BlacKKalypso/wealthings.git
```

[1]: https://developer.android.com/studio

