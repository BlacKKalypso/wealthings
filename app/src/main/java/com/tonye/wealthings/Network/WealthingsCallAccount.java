package com.tonye.wealthings.Network;


import android.util.Log;

import com.tonye.wealthings.Models.WealthingsAccount;

import java.lang.ref.WeakReference;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WealthingsCallAccount {
    public interface Callbacks {
        void onResponseAccount(WealthingsAccount account);

        void onFailureAccount();
    }

    public static void fetchAcountDetail(Callbacks callbacks, String token) {
        final WeakReference<Callbacks> callbacksWeakReference = new WeakReference<>(callbacks);

        WealthingsService wealthingsService = RetrofitClient.getRetrofitInstance().create(WealthingsService.class);

        Call<WealthingsAccount> call = wealthingsService.getAccountDetail(token);

        call.enqueue(new Callback<WealthingsAccount>() {
            @Override
            public void onResponse(Call<WealthingsAccount> call, Response<WealthingsAccount> response) {
                if (callbacksWeakReference.get() != null) {
                    callbacksWeakReference.get().onResponseAccount(response.body());
                    Log.d("CallAccount", "Code = " + response.code());
                }
            }

            @Override
            public void onFailure(Call<WealthingsAccount> call, Throwable t) {
                Log.d("CallAccount", "onFailure " + t.getMessage());
                if (callbacksWeakReference.get() != null){
                    callbacksWeakReference.get().onFailureAccount();
                }
            }
        });
    }
}
