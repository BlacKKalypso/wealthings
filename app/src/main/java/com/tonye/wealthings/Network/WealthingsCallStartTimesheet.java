package com.tonye.wealthings.Network;

import android.util.Log;

import java.lang.ref.WeakReference;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WealthingsCallStartTimesheet {
    public static void fetchStartTimesheet(BaseCallback callbacks, String token, String timesheetId) {
        final WeakReference<BaseCallback> callbacksWeakReference = new WeakReference<>(callbacks);

        WealthingsService wealthingsService = RetrofitClient.getRetrofitInstance().create(WealthingsService.class);

        Call<ResponseBody> call = wealthingsService.startTimesheet(token, timesheetId);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (callbacksWeakReference.get() != null) {
                    callbacksWeakReference.get().onResponse(response.body());
                    Log.d("CallStartTimesheet", "Code =" + response.code());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (callbacksWeakReference.get() != null) {
                    callbacksWeakReference.get().onFailure();
                    Log.d("CallStartTimesheet", "Error = "+t.getMessage());
                }
            }
        });
    }
}