package com.tonye.wealthings.Network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tonye.wealthings.Deserializer.ClientsDeserializer;
import com.tonye.wealthings.Deserializer.OrdersDeserializer;
import com.tonye.wealthings.Deserializer.TasksDeserializer;
import com.tonye.wealthings.Models.WealthingsClients;
import com.tonye.wealthings.Models.WealthingsOrders;
import com.tonye.wealthings.Models.WealthingsTasks;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    private static Retrofit retrofit;
    //private static final String BASE_URL = "https://demo.wealthings.com/api-v1/";
    private static final String BASE_URL = "http://127.0.0.1:3000/";

    public static Retrofit getRetrofitInstance(){
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit;
    }

    public static Retrofit getRetrofitInstanceClient() {
        WealthingsClients wealthingsClients = new WealthingsClients();

        if (wealthingsClients != null) {
            Gson gson = new GsonBuilder().registerTypeAdapter(WealthingsClients.class, new ClientsDeserializer()).create();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();

        } else {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        return retrofit;
    }

    public static Retrofit getRetrofitInstanceOrder(){
        WealthingsOrders wealthingsOrders = new WealthingsOrders();

        if (wealthingsOrders != null){
            Gson gson = new GsonBuilder().registerTypeAdapter(WealthingsOrders.class, new OrdersDeserializer()).create();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }else {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        return retrofit;
    }

    public static Retrofit getRetrofitInstanceTask(){
        WealthingsTasks wealthingsTasks = new WealthingsTasks();

        if (wealthingsTasks !=null){
            Gson gson = new GsonBuilder().registerTypeAdapter(WealthingsTasks.class, new TasksDeserializer()).create();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }else {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
