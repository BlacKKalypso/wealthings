package com.tonye.wealthings.Network;

import android.util.Log;

import com.tonye.wealthings.Models.WealthingsLoginInfo;

import java.lang.ref.WeakReference;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WealthingsCallLogin {
    public interface Callbacks {
        void onResponse(WealthingsLoginInfo user);

        void onFailure();
    }
    public static void fetchUser(Callbacks callbacks, String email, String password) {
        final WeakReference<Callbacks> callbacksWeakReference = new WeakReference<>(callbacks);

        WealthingsService wealthingsService = RetrofitClient.getRetrofitInstance().create(WealthingsService.class);

        Call<WealthingsLoginInfo> call = wealthingsService.getUserInfo(email, password);

        call.enqueue(new Callback<WealthingsLoginInfo>() {
            @Override
            public void onResponse(Call<WealthingsLoginInfo> call, Response<WealthingsLoginInfo> response) {
                if (callbacksWeakReference.get() != null) {
                    callbacksWeakReference.get().onResponse(response.body());
                }
            }

            @Override
            public void onFailure(Call<WealthingsLoginInfo> call, Throwable t) {
                Log.d("tag", "CallLogin error = "+t.getMessage() );
                if (callbacksWeakReference.get() != null) {
                    callbacksWeakReference.get().onFailure();
                }
            }
        });
    }
}
