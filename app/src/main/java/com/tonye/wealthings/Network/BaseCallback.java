package com.tonye.wealthings.Network;

import okhttp3.ResponseBody;

public interface BaseCallback {
        void onResponse(ResponseBody responseBody);

        void onFailure();
}
