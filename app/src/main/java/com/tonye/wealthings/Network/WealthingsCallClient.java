package com.tonye.wealthings.Network;

        import android.util.Log;

        import com.tonye.wealthings.Models.WealthingsClients;

        import java.lang.ref.WeakReference;

        import retrofit2.Call;
        import retrofit2.Callback;
        import retrofit2.Response;

public class WealthingsCallClient {
    public interface Callbacks {
        void onResponseClient(WealthingsClients clients);

        void onFailureClient();
    }

    public static void fetchClients(Callbacks callbacks, String token) {
        final WeakReference<Callbacks> callbacksWeakReference = new WeakReference<>(callbacks);

        WealthingsService wealthingsService = RetrofitClient.getRetrofitInstanceClient().create(WealthingsService.class);

        Call<WealthingsClients> call = wealthingsService.listClients(token);
        Log.d("tag", "WealthingsCallClient fetchClients "+call);
        call.enqueue(new Callback<WealthingsClients>() {
            @Override
            public void onResponse(Call<WealthingsClients> call, Response<WealthingsClients> response) {
                if (callbacksWeakReference.get() != null) {
                    callbacksWeakReference.get().onResponseClient(response.body());
                    Log.d("CallClient", "Code = "+response.code());
                }
            }

            @Override
            public void onFailure(Call<WealthingsClients> call, Throwable t) {
                Log.d("CallClient", "Error = "+t.getMessage() );
                if (callbacksWeakReference.get() != null) {
                    callbacksWeakReference.get().onFailureClient();
                }
            }
        });
    }
}
