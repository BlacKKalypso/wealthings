package com.tonye.wealthings.Network;

import com.tonye.wealthings.Models.TimesheetEntryRequest;
import com.tonye.wealthings.Models.TimesheetEntryResponse;
import com.tonye.wealthings.Models.WealthingsAccount;
import com.tonye.wealthings.Models.WealthingsClients;
import com.tonye.wealthings.Models.DayTimesheetsResponse;
import com.tonye.wealthings.Models.WealthingsLoginInfo;
import com.tonye.wealthings.Models.WealthingsOrders;
import com.tonye.wealthings.Models.WealthingsTasks;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface WealthingsService {
    String WEAL_TOKEN = "WEAL-TOKEN";

    @FormUrlEncoded
    @POST("login")
    Call<WealthingsLoginInfo> getUserInfo(
            @Field("email") String email,
            @Field("password") String password
    );

    @GET("my-timesheet/{date}")
    Call<DayTimesheetsResponse> getTimesheet(
            @Header(WEAL_TOKEN) String token,
            @Path(value = "date", encoded = true) String date
    );

    @GET("my-timesheet/client")
    Call<WealthingsClients> listClients(
            @Header(WEAL_TOKEN) String token
    );


    //TODO: Add query clientId to the endpoint
    @GET("my-timesheet/order")
    Call<WealthingsOrders> listOrders(
            @Header(WEAL_TOKEN) String token,
            @Query("clientId") int clientId
    );

    @GET("my-timesheet/employee/task")
    Call<WealthingsTasks> listTasks(
            @Header("WEAL-TOKEN") String token
    );

    @POST("my-timesheet")
    Call<TimesheetEntryResponse> postJson(
            @Header(WEAL_TOKEN) String token,
            @Body TimesheetEntryRequest body
    );

    @POST("my-timesheet/{timesheet_id}/start")
    Call<ResponseBody> startTimesheet(
            @Header(WEAL_TOKEN) String token,
            @Path("timesheet_id") String timesheetId
    );

    @POST("my-timesheet/{timesheet_id}/stop")
    Call<ResponseBody> stopTimesheet(
            @Header(WEAL_TOKEN) String token,
            @Path("timesheet_id") String timesheetId
    );

    @GET("myaccount")
    Call<WealthingsAccount> getAccountDetail(
            @Header(WEAL_TOKEN) String token
    );
}