package com.tonye.wealthings.Network;

        import android.util.Log;

        import com.tonye.wealthings.Models.WealthingsTasks;

        import java.lang.ref.WeakReference;

        import retrofit2.Call;
        import retrofit2.Callback;
        import retrofit2.Response;

public class WealthingsCallTask {
    public interface Callbacks {
        void onResponseTask(WealthingsTasks tasks);

        void onFailureTask();
    }

    public static void fetchTasks(Callbacks callbacks, String token) {
        final WeakReference<Callbacks> callbacksWeakReference = new WeakReference<>(callbacks);

        WealthingsService wealthingsService = RetrofitClient.getRetrofitInstanceTask().create(WealthingsService.class);

        Call<WealthingsTasks> call = wealthingsService.listTasks(token);

        call.enqueue(new Callback<WealthingsTasks>() {
            @Override
            public void onResponse(Call<WealthingsTasks> call, Response<WealthingsTasks> response) {
                if (callbacksWeakReference.get() != null) {
                    callbacksWeakReference.get().onResponseTask(response.body());
                    Log.d("CallTask", "Response code = "+response.code());
                }
            }

            @Override
            public void onFailure(Call<WealthingsTasks> call, Throwable t) {
                Log.d("tag", "CallTask error = "+t.getMessage() );
                if (callbacksWeakReference.get() != null) {
                    callbacksWeakReference.get().onFailureTask();
                }
            }
        });
    }
}
