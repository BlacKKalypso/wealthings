package com.tonye.wealthings.Network;

import android.util.Log;

import com.tonye.wealthings.Models.DayTimesheetsResponse;

import java.lang.ref.WeakReference;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WealthingsCallTimesheet {
    public interface Callbacks {
        void onResponse(DayTimesheetsResponse dayTimesheetsResponse);

        void onFailure();
    }

    public static void fetchTimesheet(Callbacks callbacks, String token, String date) {
        final WeakReference<Callbacks> callbacksWeakReference = new WeakReference<>(callbacks);

        WealthingsService wealthingsService = RetrofitClient.getRetrofitInstance().create(WealthingsService.class);

        Call<DayTimesheetsResponse> call = wealthingsService.getTimesheet(token, date);

        call.enqueue(new Callback<DayTimesheetsResponse>() {
            @Override
            public void onResponse(Call<DayTimesheetsResponse> call, Response<DayTimesheetsResponse> response) {
                if (callbacksWeakReference.get() != null) {
                    callbacksWeakReference.get().onResponse(response.body());
                    Log.d("CallTimesheet", "Code = "+response.code());
                }
            }

            @Override
            public void onFailure(Call<DayTimesheetsResponse> call, Throwable t) {
                Log.d("CallTimesheet", "onFailure " +t.getMessage());
                if (callbacksWeakReference.get() != null) {
                    callbacksWeakReference.get().onFailure();
                }
            }
        });
    }
}
