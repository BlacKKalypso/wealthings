package com.tonye.wealthings.Network;

import android.util.Log;

import com.tonye.wealthings.Models.TimesheetEntryRequest;
import com.tonye.wealthings.Models.TimesheetEntryResponse;

import java.lang.ref.WeakReference;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WealthingsCallNewTimesheet {
    public interface Callbacks {
        void onResponseNewTimesheet(TimesheetEntryResponse timesheetEntry);

        void onFailureNewTimesheet();
    }

    public static void fetchNewTimesheet(Callbacks callbacks, String token, TimesheetEntryRequest timesheetEntryRequest) {
        final WeakReference<WealthingsCallNewTimesheet.Callbacks> callbacksWeakReference = new WeakReference<>(callbacks);

        WealthingsService wealthingsService = RetrofitClient.getRetrofitInstance().create(WealthingsService.class);

        Call<TimesheetEntryResponse> call = wealthingsService.postJson(token, timesheetEntryRequest);

        call.enqueue(new Callback<TimesheetEntryResponse>() {
            @Override
            public void onResponse(Call<TimesheetEntryResponse> call, Response<TimesheetEntryResponse> response) {
                if (callbacksWeakReference.get() != null) {
                    callbacksWeakReference.get().onResponseNewTimesheet(response.body());
                    Log.d("CallNewTimesheet", "code = " + response.code());
                }
            }

            @Override
            public void onFailure(Call<TimesheetEntryResponse> call, Throwable t) {
                if (callbacksWeakReference.get() != null){
                    callbacksWeakReference.get().onFailureNewTimesheet();
                    Log.d("CallNewTimesheet", "Error = "+t.getMessage());
                }
            }
        });
    }
}
