package com.tonye.wealthings.Network;

import android.util.Log;

import com.tonye.wealthings.Models.WealthingsOrders;

import java.lang.ref.WeakReference;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WealthingsCallOrder {
    public interface Callbacks {
        void onResponseOrder(WealthingsOrders orders);

        void onFailureOrder();
    }

    public static void fetchOrders(Callbacks callbacks, String token, int clientId){
        final WeakReference<Callbacks> callbacksWeakReference = new WeakReference<>(callbacks);

        WealthingsService wealthingsService = RetrofitClient.getRetrofitInstanceOrder().create(WealthingsService.class);

        Call<WealthingsOrders> call = wealthingsService.listOrders(token, clientId);

        call.enqueue(new Callback<WealthingsOrders>() {
            @Override
            public void onResponse(Call<WealthingsOrders> call, Response<WealthingsOrders> response) {
                if(callbacksWeakReference.get() != null){
                    callbacksWeakReference.get().onResponseOrder(response.body());
                    Log.d("CallOrder", "Code = "+response.code());
                }
            }

            @Override
            public void onFailure(Call<WealthingsOrders> call, Throwable t) {
                if (callbacksWeakReference.get() != null){
                    callbacksWeakReference.get().onFailureOrder();
                    Log.d("CallOrder", "Error = "+t.getMessage() );
                }
            }
        });
    }
}


