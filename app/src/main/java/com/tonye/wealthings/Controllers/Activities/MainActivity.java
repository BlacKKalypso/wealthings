package com.tonye.wealthings.Controllers.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.navigation.NavigationView;
import com.tonye.wealthings.Controllers.Fragments.HomeFragment;
import com.tonye.wealthings.Controllers.Fragments.TimesheetsFragment;
import com.tonye.wealthings.Controllers.Fragments.SettingsFragment;
import com.tonye.wealthings.R;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;

    private Fragment fragmentHome;
    private Fragment fragmentTimesheets;
    private Fragment fragmentSettings;

    //Id for each fragment
    public static final int FRAGMENT_HOME = 0;
    public static final int FRAGMENT_TIMESHEETS = 1;
    public static final int FRAGMENT_SETTINGS = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Configuration of views
        this.setToolbar();
        this.setDrawerLayout();
        this.setNavigationView();

        //Display HomeFragment
        this.displayFirstFragment();
    }

    @Override
    public void onBackPressed() {
        if (this.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            this.drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        //Handle Navigation Item Click
        int id = menuItem.getItemId();

        switch (id) {
            case R.id.drawer_home:
                this.displayHomeFragment();
                break;
            case R.id.drawer_timesheets:
                this.displayTimesheetsFragment();
                break;
            case R.id.drawer_settings:
                this.displaySettingsFragment();
                break;
            default:
                break;
        }

        this.drawerLayout.closeDrawer(GravityCompat.START);

        return true;
    }

    /*----------------------------------------*
     * SET UP
     * -------------------------------------*/

    //Toolbar
    private void setToolbar() {
        this.toolbar = findViewById(R.id.toolbar);
        this.toolbar.setTitle(getString(R.string.home_title));
        setSupportActionBar(toolbar);
    }

    //Drawer Layout
    private void setDrawerLayout() {
        this.drawerLayout = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
    }

    //NavigationView
    private void setNavigationView() {
        this.navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    /*----------------------------------------*
     * FRAGMENTS
     * ---------------------------------------*/

    private void displayFirstFragment() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame_layout);
        if (fragment == null) {
            this.displayFragment(FRAGMENT_HOME);
            this.navigationView.getMenu().getItem(0).setChecked(true);
        }
    }

    //Display fragment according an identifier
    private void displayFragment(int fragmentIdentifier) {
        switch (fragmentIdentifier) {
            case FRAGMENT_HOME:
                this.displayHomeFragment();
                break;
            case FRAGMENT_TIMESHEETS:
                this.displayTimesheetsFragment();
                break;
            case FRAGMENT_SETTINGS:
                this.displaySettingsFragment();
                break;
            default:
                break;
        }
    }

    private void displayHomeFragment() {
        if (this.fragmentHome == null) {
            this.fragmentHome = HomeFragment.newInstance();
        }
        this.startTransactionFragment(this.fragmentHome);
        this.toolbar.setTitle(getString(R.string.home_title));

    }

    private void displayTimesheetsFragment() {
        if (this.fragmentTimesheets == null) {
            this.fragmentTimesheets = TimesheetsFragment.newInstance();
        }
        this.startTransactionFragment(this.fragmentTimesheets);
        this.toolbar.setTitle(getString(R.string.timesheet_title));

    }

    private void displaySettingsFragment() {
        if (this.fragmentSettings == null) {
            this.fragmentSettings = SettingsFragment.newInstance();
        }
        this.startTransactionFragment(this.fragmentSettings);
        this.toolbar.setTitle(getString(R.string.settings_title));
    }

    private void startTransactionFragment(Fragment fragment) {
        if (!fragment.isVisible()) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.frame_layout, fragment).commit();
        }
    }
}
