package com.tonye.wealthings.Controllers.Fragments;

import android.graphics.Typeface;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tonye.wealthings.Models.DayTimesheet;
import com.tonye.wealthings.Models.DayTimesheetsResponse;
import com.tonye.wealthings.Models.TimeEntry;
import com.tonye.wealthings.Network.WealthingsCallTimesheet;
import com.tonye.wealthings.R;
import com.tonye.wealthings.Utils.PopupMessage;
import com.tonye.wealthings.Utils.SaveSharedPreference;
import com.tonye.wealthings.Utils.VariablesUtil;
import com.tonye.wealthings.Views.WealthingsDayEntriesAdapter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.Disposable;

/**
 * A simple {@link Fragment} subclass.
 */
public class TimesheetsFragment extends Fragment implements WealthingsCallTimesheet.Callbacks, View.OnClickListener {
    @BindView(R.id.day_recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.header_title)
    TextView headerDate;
    @BindView(R.id.header_previous_day)
    Button btnPreviousDay;
    @BindView(R.id.header_next_day)
    Button btnNextDay;

    //FOR DATA
    private Disposable disposable;
    private List<TimeEntry> timeEntries;
    private WealthingsDayEntriesAdapter adapter;
    private String token;
    private Date displayDate;
    private String displayDateString;

    public static TimesheetsFragment newInstance() {
        return new TimesheetsFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_timesheets, container, false);
        ButterKnife.bind(this, view);

        Typeface icon = Typeface.createFromAsset(view.getContext().getAssets(), "icomoon.ttf");

        this.btnNextDay.setTypeface(icon, Typeface.BOLD);
        this.btnPreviousDay.setTypeface(icon, Typeface.BOLD);

        this.btnNextDay.setOnClickListener(this::onClick);
        this.btnPreviousDay.setOnClickListener(this::onClick);

        this.btnNextDay.setVisibility(View.INVISIBLE);

        Date now = new Date();
        displayDate = this.setUpDayBefore(now);
        displayDate = this.setTimeToZero(displayDate);

        displayDateString = this.convertDateToString(displayDate);

        token = SaveSharedPreference.getTokenKey(view.getContext());

        this.updateHeaderDate();
        this.setUpRecyclerView();
        this.fetchTimesheet();

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.disposeDestroy();
    }

    private void fetchTimesheet() {
        WealthingsCallTimesheet.fetchTimesheet(this, this.token, convertDateToString(this.displayDate));
    }

    @Override
    public void onResponse(DayTimesheetsResponse dayTimesheetsResponse) {
        try {
            if (dayTimesheetsResponse != null) {
                List<DayTimesheet> dayTimesheetList = dayTimesheetsResponse.getDayTimesheets();

                for (int i = 0; i < dayTimesheetList.size(); i++) {
                    String timesheetDate = dayTimesheetList.get(i).getDate();
                    if (displayDateString.compareTo(timesheetDate) == 0) {
                        this.updateUi(dayTimesheetList.get(i).getTimeEntries());
                        break;

                    } else if (dayTimesheetList.get(i) == dayTimesheetList.get(dayTimesheetList.size() -1)) {
                        PopupMessage.update(getContext(), "No timesheet");
                        this.cleanTimeEntriesList();
                    }
                }

            } else {
                PopupMessage.update(getContext(), "No timesheet");
            }
        } catch (Exception e) {
            Log.d("Error", "Timesheets past day: " + e.getMessage());
        }
    }

    @Override
    public void onFailure() {
        //TODO: Add toast, dialog text or update Notimesheet
    }

    private Date setTimeToZero(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return cal.getTime();
    }

    private void disposeDestroy() {
        if (this.disposable != null && !this.disposable.isDisposed()) {
            this.disposable.dispose();
        }
    }

    private String convertDateToString(Date date) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        String dateString = dateFormat.format(date);
        String dateStingSplit = dateString.split("T")[0];
        dateString = dateStingSplit + VariablesUtil.DEFAULT_TIME;
        return dateString;
    }

    private Date setUpDayBefore(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, -1);

        Date dayBefore = c.getTime();

        return dayBefore;
    }

    private Date setUpDayAfter(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, 1);

        Date dayAfter = c.getTime();

        return dayAfter;
    }

    //3.
    private void updateHeaderDate() {
        String yesterday = convertDateToString(this.displayDate);
        String yesterdaySplit = yesterday.split("T")[0];
        this.headerDate.setText(yesterdaySplit);
    }

    private void updateUi(List<TimeEntry> entries) {
        timeEntries.addAll(entries);
        adapter.notifyDataSetChanged();
    }

    private void cleanTimeEntriesList() {
        this.timeEntries.clear();
        adapter.notifyDataSetChanged();
    }

    private void setUpRecyclerView() {
        this.timeEntries = new ArrayList<>();

        this.adapter = new WealthingsDayEntriesAdapter(this.timeEntries);

        this.recyclerView.setAdapter(this.adapter);
        this.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        this.recyclerView.addItemDecoration(new DividerItemDecoration(this.getActivity(), LinearLayout.VERTICAL));
    }

    @Override
    public void onClick(View v) {
        try {
            switch (v.getId()) {
                case R.id.header_previous_day:
                    this.displayDate = setUpDayBefore(this.displayDate);
                    this.displayDateString = this.convertDateToString(this.displayDate);
                    btnNextDay.setVisibility(View.VISIBLE);
                    break;

                case R.id.header_next_day:
                    this.displayDate = setUpDayAfter(this.displayDate);
                    this.displayDateString = this.convertDateToString(this.displayDate);

                    Date displayDatePlusOneDay = setUpDayAfter(this.displayDate);
                    Date today = new Date();
                    today = this.setTimeToZero(today);


                    if (displayDatePlusOneDay.compareTo(today) == 0) {
                        btnNextDay.setVisibility(View.INVISIBLE);
                    }
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            PopupMessage.update(v.getContext(), getString(R.string.failure_next_date));
        }

        cleanTimeEntriesList();
        fetchTimesheet();
        updateHeaderDate();
    }
}
