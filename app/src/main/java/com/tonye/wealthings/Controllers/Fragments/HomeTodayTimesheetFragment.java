package com.tonye.wealthings.Controllers.Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.tonye.wealthings.Models.DayTimesheet;
import com.tonye.wealthings.Models.DayTimesheetsResponse;
import com.tonye.wealthings.Models.TimeEntry;
import com.tonye.wealthings.Network.BaseCallback;
import com.tonye.wealthings.Network.WealthingsCallStartTimesheet;
import com.tonye.wealthings.Network.WealthingsCallStopTimesheet;
import com.tonye.wealthings.Network.WealthingsCallTimesheet;
import com.tonye.wealthings.R;
import com.tonye.wealthings.Utils.ItemClickSupport;
import com.tonye.wealthings.Utils.PopupMessage;
import com.tonye.wealthings.Utils.SaveSharedPreference;
import com.tonye.wealthings.Utils.VariablesUtil;
import com.tonye.wealthings.Views.WealthingsTodayEntriesAdapter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.Disposable;
import okhttp3.ResponseBody;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeTodayTimesheetFragment extends Fragment implements WealthingsCallTimesheet.Callbacks,
        WealthingsTodayEntriesAdapter.Listener {
    //FOR DESIGN
    @BindView(R.id.home_recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.home_swipe_container)
    SwipeRefreshLayout swipeRefreshLayout;

    //DATA
    private List<TimeEntry> timeEntries;
    private WealthingsTodayEntriesAdapter adapter;
    private String token;
    private Date today;
    private String todayString;
    private Disposable disposable;
    private BaseCallback startStopCallback = new BaseCallback() {
        @Override
        public void onResponse(ResponseBody responseBody) {
            fetchTimesheet();
        }

        @Override
        public void onFailure() {

        }
    };

    public HomeTodayTimesheetFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home_today, container, false);
        ButterKnife.bind(this, view);

        today = new Date();
        todayString = this.convertDateToString(today);
        today = this.setTimeToZero(today);


        token = SaveSharedPreference.getTokenKey(view.getContext());

        this.setUpSwipeRefreshLayout();
        this.setUpRecyclerView();
        this.setUpOnClickRecyclerView();
        this.fetchTimesheet();
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.disposeDestroy();
    }

    private String convertDateToString(Date date) {
        DateFormat dateFormat = new SimpleDateFormat(VariablesUtil.DEFAULT_DATE);
        String dateString = dateFormat.format(date);
        String dateStringSplit = dateString.split("T")[0];
        dateString = dateStringSplit + VariablesUtil.DEFAULT_TIME;

        return dateString;
    }

    private void fetchTimesheet() {
        WealthingsCallTimesheet.fetchTimesheet(this, this.token, convertDateToString(this.today));
    }

    @Override
    public void onResponse(DayTimesheetsResponse dayTimesheetsResponse) {

        if (dayTimesheetsResponse != null) {
            Log.d("DaTimesheet", "Timesheet " + dayTimesheetsResponse.getDayTimesheets());
            for (DayTimesheet dayTimesheet : dayTimesheetsResponse.getDayTimesheets()) {
                if (!dayTimesheetsResponse.getDayTimesheets().isEmpty() && todayString.compareTo(dayTimesheet.getDate()) == 0) {
                    this.updateUI(dayTimesheet.getTimeEntries());
                    break;
                } else {
                    NoTimesheetFragment noTimesheetFragment = new NoTimesheetFragment();
                    getActivity().getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.home_fragment_container, noTimesheetFragment)
                            .commit();
                }
            }

        } else {
            NoTimesheetFragment noTimesheetFragment = new NoTimesheetFragment();
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.home_fragment_container, noTimesheetFragment)
                    .commit();
        }
    }

    @Override
    public void onFailure() {
        PopupMessage.update(getContext(), getString(R.string.failure_connexion));
    }

    private void disposeDestroy() {
        if (this.disposable != null && !this.disposable.isDisposed()) {
            this.disposable.dispose();
        }
    }

    public Date setTimeToZero(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return cal.getTime();
    }

    private void updateUI(List<TimeEntry> entries) {
        //When the request correctly end, refresh animation end
        swipeRefreshLayout.setRefreshing(false);
        //Avoid time entries duplicating because of addAll()
        timeEntries.clear();
        this.sortedTimeEntry(entries);
        timeEntries.addAll(entries);
        adapter.notifyDataSetChanged();
    }

    private void setUpRecyclerView() {
        //Reset list
        this.timeEntries = new ArrayList<>();
        //Create adapter passing the list of users
        this.adapter = new WealthingsTodayEntriesAdapter(this.timeEntries, this);
        //Attach the adapter to the recyclerview to populate items
        this.recyclerView.setAdapter(this.adapter);
        //Set layout manager to position the items
        this.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        this.recyclerView.addItemDecoration(new DividerItemDecoration(this.getActivity(), LinearLayout.VERTICAL));
    }

    private void setRunningTimeEntry(int position) {
        for (TimeEntry timeEntry : timeEntries) {
            if (timeEntries.indexOf(timeEntry) == position) {
                timeEntry.setEndTime(null);
            } else if (timeEntry.isRunning()) {
                timeEntry.setEndTime(new Date());
            }
        }
    }

    private void sortedTimeEntry(List<TimeEntry> entries) {
        if (entries.size() > 0) {
            Collections.sort(entries, new Comparator<TimeEntry>() {
                @Override
                public int compare(TimeEntry o1, TimeEntry o2) {
                    return o1.getId().compareTo(o2.getId());
                }
            });
        }
    }

    /**
     * -----------------------
     * ACTIONS
     * -------------------------
     */

    private void setUpSwipeRefreshLayout() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchTimesheet();
            }
        });
    }

    private void setUpOnClickRecyclerView() {
        ItemClickSupport.addTo(recyclerView, R.layout.fragment_home_today_item)
                .setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                    @Override
                    public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                        Log.d("tag", "Position : " + position);
                        //Get entry from adapter
                        TimeEntry entry = adapter.getEntry(position);
                    }
                });
    }

    @Override
    public void onClickPlayButton(int position) {
        TimeEntry entry = adapter.getEntry(position);
        this.setRunningTimeEntry(position);
        adapter.notifyDataSetChanged();
        WealthingsCallStartTimesheet.fetchStartTimesheet(startStopCallback, this.token, entry.getId());
    }

    @Override
    public void onClickPauseButton(int position) {
        TimeEntry entry = adapter.getEntry(position);
        WealthingsCallStopTimesheet.fetchStopTimesheet(startStopCallback, this.token, entry.getId());
    }
}
