package com.tonye.wealthings.Controllers.Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tonye.wealthings.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class NoTimesheetFragment extends Fragment {
    @BindView(R.id.home_no_swipe_container)
    SwipeRefreshLayout swipeRefreshLayout;

    public NoTimesheetFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_no_timesheet, container, false);
        ButterKnife.bind(this, view);

        this.setUpRecyclerView();

        return view;
    }

    private void setUpRecyclerView(){
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                HomeTodayTimesheetFragment homeTodayTimesheetFragment = new HomeTodayTimesheetFragment();
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.home_fragment_container, homeTodayTimesheetFragment)
                        .commit();
            }
        });
    }
}
