package com.tonye.wealthings.Controllers.Fragments;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.tonye.wealthings.Models.WealthingsLoginInfo;
import com.tonye.wealthings.Network.WealthingsCallLogin;
import com.tonye.wealthings.R;
import com.tonye.wealthings.Utils.PopupMessage;
import com.tonye.wealthings.Utils.SaveSharedPreference;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFormFragment extends Fragment implements WealthingsCallLogin.Callbacks{

    //DATA
    private EditText edtEmail;
    private EditText edtPassword;
    //private EditText edtTotpkey;
    private Button button;

    public LoginFormFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login_form, container, false);

        //Display splash instead of login form
        if (SaveSharedPreference.getLoggedStatus(view.getContext())){
            LoginSplashFragment splashFragment = new LoginSplashFragment();
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.login_fragment_container, splashFragment, "splashFragment")
                    .addToBackStack(null)
                    .commit();
        }

        edtEmail = view.findViewById(R.id.edtEmail);
        edtPassword = view.findViewById(R.id.edtPassword);
        //edtTotpkey = view.findViewById(R.id.edtTotpkey);

        //edtEmail.addTextChangedListener(loginTextWatcher);
        //edtPassword.addTextChangedListener(loginTextWatcher);
        //edtTotpkey.addTextChangedListener(loginTextWatcher);
        button = view.findViewById(R.id.btn_login);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                executeHttpRequest();
            }
        });

        return view;
    }

    //Disable button until fields are filled

    /*
    private TextWatcher loginTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String emailInput = edtEmail.getText().toString().trim();
            String passwordInput = edtPassword.getText().toString().trim();
            //String totpkeyInput = edtTotpkey.getText().toString().trim();

            button.setEnabled(!emailInput.isEmpty() && !passwordInput.isEmpty());
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };
     */

    private void executeHttpRequest() {
        WealthingsCallLogin.fetchUser(this, this.edtEmail.getText().toString(), this.edtPassword.getText().toString());
    }

    @Override
    public void onResponse(@Nullable WealthingsLoginInfo user) {
        Log.d("toto", "toto");
        Log.d("user", String.valueOf(user));
        if (user != null && user.getToken() != null) {
            this.updateFragment(user);
        } else {
            PopupMessage.update(getContext(), getString(R.string.failure_login));
        }
    }

    @Override
    public void onFailure() {
        Log.d("oto", "toto");
        PopupMessage.update(getContext(),getString(R.string.failure_login));
    }

    private void updateFragment(WealthingsLoginInfo user) {
        String token = user.getToken();
        if (token != null) {
            SaveSharedPreference.setLoggedIn(getActivity(), true);
            SaveSharedPreference.setTokenKey(getActivity(), token);

            LoginSplashFragment splashFragment = new LoginSplashFragment();
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.login_fragment_container, splashFragment, "splashFragment")
                    .addToBackStack(null)
                    .commit();
        } else {
            PopupMessage.update(getContext(), getString(R.string.failure_connexion));
        }
    }
}
