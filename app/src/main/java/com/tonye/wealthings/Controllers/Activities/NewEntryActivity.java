package com.tonye.wealthings.Controllers.Activities;

import androidx.appcompat.app.AppCompatActivity;


import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import com.tonye.wealthings.Models.TimesheetEntryRequest;
import com.tonye.wealthings.Models.TimesheetEntryResponse;
import com.tonye.wealthings.Models.WealthingsClient;
import com.tonye.wealthings.Models.WealthingsClients;
import com.tonye.wealthings.Models.WealthingsOrder;
import com.tonye.wealthings.Models.WealthingsOrders;
import com.tonye.wealthings.Models.WealthingsTask;
import com.tonye.wealthings.Models.WealthingsTasks;
import com.tonye.wealthings.Network.WealthingsCallClient;
import com.tonye.wealthings.Network.WealthingsCallNewTimesheet;
import com.tonye.wealthings.Network.WealthingsCallOrder;
import com.tonye.wealthings.Network.WealthingsCallTask;
import com.tonye.wealthings.R;
import com.tonye.wealthings.Utils.PopupMessage;
import com.tonye.wealthings.Utils.SaveSharedPreference;
import com.tonye.wealthings.Utils.VariablesUtil;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NewEntryActivity extends AppCompatActivity implements View.OnClickListener,
        WealthingsCallClient.Callbacks,
        WealthingsCallOrder.Callbacks,
        WealthingsCallTask.Callbacks,
        WealthingsCallNewTimesheet.Callbacks,
        CompoundButton.OnCheckedChangeListener {
    @BindView(R.id.switch_billable)
    Switch switchBill;
    @BindView(R.id.spinner_clients)
    Spinner spClients;
    @BindView(R.id.spinner_orders)
    Spinner spOrders;
    @BindView(R.id.spinner_tasks)
    Spinner spTasks;
    @BindView(R.id.edt_description)
    EditText edtDesc;
    @BindView(R.id.edt_time)
    EditText edtTime;
    @BindView(R.id.btn_start)
    Button btnStart;
    @BindView(R.id.date_picker_choice)
    Button dateChoice;
    private Toolbar toolbar;
    @BindView(R.id.txt_date)
    TextView txtDate;


    //DATA
    private String token;
    private int year;
    private int month;
    private int day;
    private Date todayDate;
    private TimesheetEntryRequest request = new TimesheetEntryRequest();
    private WealthingsClient wealthingsClients;
    private WealthingsOrder wealthingsOrders;
    private WealthingsTask wealthingsTasks;
    private ArrayAdapter spinnerArrayAdapter;
    private int clientId;
    private String tag = "New time Entry";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_entry);
        ButterKnife.bind(this);

        this.setToolbar();
        switchBill.setChecked(true);

        this.token = SaveSharedPreference.getTokenKey(this.getApplicationContext());
        Log.d(this.tag, "token =" + token);

        Typeface icon = Typeface.createFromAsset(this.getAssets(), "icomoon.ttf");
        this.txtDate.setTypeface(icon);

        //Format calendar at today
        todayDate = new Date();
        String todayTxt = convertDateToString(todayDate);
        String todayTxtSplit = todayTxt.split("T")[0];
        dateChoice.setText(todayTxtSplit);
        request.setDate(convertDateToString(todayDate));

        //Switch case
        if (this.switchBill != null) {
            this.switchBill.setOnCheckedChangeListener(this);
        }

        edtDesc.addTextChangedListener(inputTextWatcher);
        edtTime.addTextChangedListener(inputTextWatcher);

        this.executeHttpRequest();

        dateChoice.setOnClickListener(this);
        btnStart.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        //Calendar to select a date and default set to today date
        switch (v.getId()) {
            case R.id.date_picker_choice:
                Calendar c = Calendar.getInstance();
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(NewEntryActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        dateChoice.setText(year + "-" + (month + 1) + "-" + dayOfMonth);
                        request.setDate(dateChoice.getText().toString() + VariablesUtil.DEFAULT_TIME);
                    }
                }, year, month, day);
                datePickerDialog.show();
                break;
            case R.id.btn_start:
                WealthingsCallNewTimesheet.fetchNewTimesheet(this, this.token, this.request);

                Intent intent = new Intent(NewEntryActivity.this, MainActivity.class);
                startActivity(intent);
                break;

            default:
                break;
        }
    }

    /*=================================================
     * RETROFIT REQUESTS
     * =================================================*/

    private void executeHttpRequest() {
        WealthingsCallClient.fetchClients(this, this.token);
        WealthingsCallTask.fetchTasks(this, this.token);
    }

    private void fetchOrders(){
        WealthingsCallOrder.fetchOrders(this, this.token, this.clientId);
    }

    @Override
    public void onResponseClient(WealthingsClients clients) {
        if (clients != null && clients.getClients() != null) {
            Log.d(this.tag, "newEntry Clients =" + clients.getClients());
            this.setSpinnerClient(clients.getClients());
            this.selectedSpClient(clients.getClients());
        }
    }

    @Override
    public void onFailureClient() {
        //TODO: Failure case
    }

    @Override
    public void onResponseOrder(WealthingsOrders orders) {
        if (orders != null && orders.getOrders() != null && this.clientId != 0) {
            Log.d(this.tag, "Orders = " + orders.getOrders());
            this.setSpinnerOrder(orders.getOrders());
            this.selectedSpOrder(orders.getOrders());
        }
    }

    @Override
    public void onFailureOrder() {
        //TODO: Failure case
    }

    @Override
    public void onResponseTask(WealthingsTasks tasks) {
        if (tasks != null && tasks.getTasks() != null) {
            this.setSpinnerTask(tasks.getTasks());
            this.selectedTask(tasks.getTasks());
        }
    }

    @Override
    public void onFailureTask() {
        //TODO: Failure case
    }

    @Override
    public void onResponseNewTimesheet(TimesheetEntryResponse timesheetEntry) {

    }

    @Override
    public void onFailureNewTimesheet() {
        //TODO: Failure case
    }

    private void setToolbar() {
        this.toolbar = findViewById(R.id.toolbar_new_entry);
        this.toolbar.setTitle(getString(R.string.entry_title));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private String convertDateToString(Date date) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        String dateString = dateFormat.format(date);
        String dateStingSplit = dateString.split("T")[0];
        dateString = dateStingSplit + VariablesUtil.DEFAULT_TIME;
        return dateString;
    }
    /*=================================================
     * SPINNERS
     * =================================================*/

    //CONFIGURATION
    private void setSpinnerClient(List<WealthingsClient> clients) {
        this.spinnerArrayAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, clients);
        this.spClients.setAdapter(spinnerArrayAdapter);
    }

    private void setSpinnerOrder(List<WealthingsOrder> orders) {
        this.spinnerArrayAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, orders);
        this.spOrders.setAdapter(spinnerArrayAdapter);
    }

    private void setSpinnerTask(List<WealthingsTask> tasks) {
        this.spinnerArrayAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, tasks);

        this.spTasks.setAdapter(spinnerArrayAdapter);
    }

    private void setNoSelectionSpinner() {
        this.spinnerArrayAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item);
        spinnerArrayAdapter.add("No selection");
        this.spTasks.setAdapter(spinnerArrayAdapter);
    }

    //SELECTED ITEM
    private void selectedSpClient(List<WealthingsClient> clients) {
        this.wealthingsClients = new WealthingsClient();

        this.spClients.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String clientName = spClients.getSelectedItem().toString();
                Log.d(tag, "ClientName =" + clientName);
                for (WealthingsClient client : clients) {
                    Log.d(tag, "client name = " + client.getContactName());
                    if (clientName == client.getContactName()) {
                        Log.d(tag, "ClientName");
                        request.setClientId(client.getContactId());
                        clientId = Integer.parseInt(client.getContactId());
                        fetchOrders();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                PopupMessage.update(getBaseContext(), "No client selected");
            }
        });

    }

    private void selectedSpOrder(List<WealthingsOrder> orders) {
        this.wealthingsOrders = new WealthingsOrder();
        this.spOrders.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String orderName = spOrders.getSelectedItem().toString();
                for (WealthingsOrder order : orders) {
                    if (orderName == order.getName()) {
                        request.setOrderId(order.getId());
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //TODO: Add a Toast or dialog
            }
        });
    }

    private void selectedTask(List<WealthingsTask> tasks) {
        this.wealthingsTasks = new WealthingsTask();
        this.spTasks.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String taskName = spTasks.getSelectedItem().toString();
                for (WealthingsTask task : tasks) {
                    if (taskName == task.getName()) {
                        request.setTaskId(task.getId());
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                setNoSelectionSpinner();
                request.setTaskId(null);
            }
        });
    }

    //Set switch button
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        //isChecked is a Boolean
        if (isChecked) {
            //Do stuff when Switch is on
            this.request.setIsBillable(true);
        } else {
            //do stuff when Switch is off
            this.request.setIsBillable(false);
        }
    }

    //Listener on edit Text description
    private TextWatcher inputTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String descInput = edtDesc.getText().toString().trim();

            if (!edtTime.getText().toString().isEmpty()) {
                String timeInput = edtTime.getText().toString().trim();
                float time = Float.parseFloat(timeInput);
                request.setTime(String.format("%.2f", time));
            }

            request.setDescription(descInput);

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };
}
