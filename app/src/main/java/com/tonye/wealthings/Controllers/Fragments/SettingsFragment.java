package com.tonye.wealthings.Controllers.Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.tonye.wealthings.Models.WealthingsAccount;
import com.tonye.wealthings.Network.WealthingsCallAccount;
import com.tonye.wealthings.R;
import com.tonye.wealthings.Utils.PopupMessage;
import com.tonye.wealthings.Utils.SaveSharedPreference;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends Fragment implements WealthingsCallAccount.Callbacks{
    @BindView(R.id.edt_settings_last_name)
    EditText edtLastName;
    @BindView(R.id.edt_settings_first_name)
    EditText edtFirstName;
    @BindView(R.id.edt_settings_email)
    EditText edtEmail;
    @BindView(R.id.edt_settings_token)
    EditText edtToken;

    private String token;

    public static SettingsFragment newInstance() {
        return new SettingsFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        ButterKnife.bind(this, view);

        token = SaveSharedPreference.getTokenKey(view.getContext());

        this.fetchAccountDetail();
        return view;
    }

    private void fetchAccountDetail(){
        WealthingsCallAccount.fetchAcountDetail(this, this.token);
    }

    @Override
    public void onResponseAccount(WealthingsAccount account) {
        if (account != null){
            this.edtLastName.setText(account.getLastName());
            this.edtFirstName.setText(account.getFirstName());
            this.edtEmail.setText(account.getEmail());
            this.edtEmail.setSelection(edtEmail.getText().length());
            this.edtToken.setText(account.getToken());
        }
    }



    @Override
    public void onFailureAccount() {
        PopupMessage.update(getContext(), getString(R.string.failure_connexion));
    }
}
