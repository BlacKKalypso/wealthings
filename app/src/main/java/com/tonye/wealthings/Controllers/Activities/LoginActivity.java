package com.tonye.wealthings.Controllers.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;

import com.tonye.wealthings.Controllers.Fragments.LoginFormFragment;
import com.tonye.wealthings.R;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        this.firstFragment();
    }

    private void firstFragment(){
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().add(R.id.login_fragment_container, new LoginFormFragment(), "LoginFormFragment").commit();
    }
}
