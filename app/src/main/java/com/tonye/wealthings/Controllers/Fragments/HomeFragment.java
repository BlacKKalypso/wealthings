package com.tonye.wealthings.Controllers.Fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.tonye.wealthings.Controllers.Activities.NewEntryActivity;
import com.tonye.wealthings.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {
    @BindView(R.id.fab) FloatingActionButton fab;

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);

        fab.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), NewEntryActivity.class);
                startActivity(intent);
            }
        });
        this.firstFragment();

        return view;
    }

    private void firstFragment(){
        HomeTodayTimesheetFragment dayTimesheetFragment = new HomeTodayTimesheetFragment();

        //TODO: Maybe addToBackStack(null)
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container, dayTimesheetFragment)
                .commit();
    }
}
