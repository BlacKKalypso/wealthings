package com.tonye.wealthings.Controllers.Fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tonye.wealthings.Controllers.Activities.MainActivity;
import com.tonye.wealthings.R;
import com.tonye.wealthings.Utils.SaveSharedPreference;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginSplashFragment extends Fragment {

    public LoginSplashFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login_splash, container, false);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (SaveSharedPreference.getLoggedStatus(getActivity())) {
                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    startActivity(intent);
                }
            }
        }, 1000);
    }
}
