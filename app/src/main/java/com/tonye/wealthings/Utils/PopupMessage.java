package com.tonye.wealthings.Utils;

import android.content.Context;
import android.widget.Toast;

public class PopupMessage {
    public static void update(Context context, String msg){
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }
}
