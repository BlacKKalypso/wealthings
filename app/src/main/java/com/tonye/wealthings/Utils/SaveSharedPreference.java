package com.tonye.wealthings.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import static com.tonye.wealthings.Utils.PreferencesUtility.LOGGED_IN_PREF;
import static com.tonye.wealthings.Utils.PreferencesUtility.TOKEN_PREF;

public class SaveSharedPreference {
    static SharedPreferences getPreferences(Context context){
        return context.getSharedPreferences("shared_prefs", Context.MODE_PRIVATE);
    }

    /**
     * Set Login Status
     * @param context
     * @param loggedIn
     * */

    public static void setLoggedIn(Context context, boolean loggedIn){
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putBoolean(LOGGED_IN_PREF, loggedIn);
        editor.apply();
    }

    public static boolean getLoggedStatus(Context context) {
        return getPreferences(context).getBoolean(LOGGED_IN_PREF, false);
    }

    /**
     * Set Token for consuming api
     * @param context
     * @param token
     * */

    public static void setTokenKey(Context context, String token){
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putString(TOKEN_PREF, token);
        editor.apply();
    }

    public static String getTokenKey(Context context) {
        return getPreferences(context).getString(TOKEN_PREF, null);
    }
}
