package com.tonye.wealthings.Models;

import java.util.List;

public class DayTimesheetsResponse {
    private List<DayTimesheet> dayTimesheets;
    private Boolean locked;
    private double total;

    public List<DayTimesheet> getDayTimesheets() {
        return dayTimesheets;
    }

    public void setDayTimesheets(List<DayTimesheet> dayTimesheets) {
        this.dayTimesheets = dayTimesheets;
    }

    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
}
