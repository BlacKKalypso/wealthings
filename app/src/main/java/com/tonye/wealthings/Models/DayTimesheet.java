package com.tonye.wealthings.Models;

import java.util.List;

public class DayTimesheet {
    private String date;
    private List<TimeEntry> timeEntries;
    private boolean locked;
    private double total;

    public List<TimeEntry> getTimeEntries() {
        return timeEntries;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setTimeEntries(List<TimeEntry> timeEntries) {
        this.timeEntries = timeEntries;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
}
