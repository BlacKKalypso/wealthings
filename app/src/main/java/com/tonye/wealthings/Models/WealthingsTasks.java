package com.tonye.wealthings.Models;

import java.util.List;

public class WealthingsTasks {
    List<WealthingsTask> tasks;

    public List<WealthingsTask> getTasks() {
        return tasks;
    }

    public void setTasks(List<WealthingsTask> tasks) {
        this.tasks = tasks;
    }
}
