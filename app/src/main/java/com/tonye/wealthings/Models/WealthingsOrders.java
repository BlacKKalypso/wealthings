package com.tonye.wealthings.Models;

import java.util.List;

public class WealthingsOrders {
    private List<WealthingsOrder> orders;

    public List<WealthingsOrder> getOrders() {
        return orders;
    }

    public void setOrders(List<WealthingsOrder> orders) {
        this.orders = orders;
    }
}
