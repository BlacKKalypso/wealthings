package com.tonye.wealthings.Models;

import java.util.Date;

public class TimeEntry {
    private String id;
    private String date;
    private String clientName;
    private String orderName;
    private float time = 0;
    private String description;
    private Date startTime;
    private Date endTime;

    public String getId(){ return id; }

    public void setId(String id){ this.id = id; }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public float getTime() {
        return time;
    }

    public void setTime(float time) {
        this.time = time;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public boolean isRunning(){
        if (endTime == null){
            return true;
        } else {
            return false;
        }
    }
}
