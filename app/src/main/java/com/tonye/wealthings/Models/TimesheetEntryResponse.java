package com.tonye.wealthings.Models;

import com.google.gson.annotations.SerializedName;

//Model to create a new Timesheet
public class TimesheetEntryResponse {
    private Data data;
    @SerializedName("error")
    private ErrorObj error;
    private Boolean valid;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public ErrorObj getError() {
        return error;
    }

    public void setError(ErrorObj error) {
        this.error = error;
    }

    public Boolean getValid() {
        return valid;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }
}
