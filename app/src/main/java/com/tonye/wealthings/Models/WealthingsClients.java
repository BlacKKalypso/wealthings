package com.tonye.wealthings.Models;

import java.util.List;

public class WealthingsClients {
    private List<WealthingsClient> clients;

    public List<WealthingsClient> getClients() {
        return clients;
    }

    public void setClients(List<WealthingsClient> clients) {
        this.clients = clients;
    }
}
