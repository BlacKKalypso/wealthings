package com.tonye.wealthings.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WealthingsLoginInfo {
    @SerializedName("contactId")
    @Expose
    private int contactId;

    @SerializedName("avatarId")
    @Expose
    private int avatarId;

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("organizationId")
    @Expose
    private  int organizationId;

    @SerializedName("firstName")
    @Expose
    private String firstName;

    @SerializedName("middleName")
    @Expose
    private String middleName;

    @SerializedName("lastName")
    @Expose
    private String lastName;

    @SerializedName("token")
    @Expose
    private String token;

    public int getContactId() {
        return contactId;
    }

    public void setContactId(int contactId) {
        this.contactId = contactId;
    }

    public int getAvatarId() {
        return avatarId;
    }

    public void setAvatarId(int avatarId) {
        this.avatarId = avatarId;
    }

    public int getId() {
        return id;
    }

    public int getOrganizationId() {
        return organizationId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getToken(){ return token; }

    public void setId(int id) {
        this.id = id;
    }

    public void setOrganizationId(int organizationId) {
        this.organizationId = organizationId;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
