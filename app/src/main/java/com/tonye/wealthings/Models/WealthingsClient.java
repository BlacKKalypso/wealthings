package com.tonye.wealthings.Models;

import androidx.annotation.NonNull;

public class WealthingsClient {
    private String contactId;
    private String contactName;

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public String getContactName() {
        return this.contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    @NonNull
    @Override
    public String toString() {
        return contactName;
    }
}
