package com.tonye.wealthings.Deserializer;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import com.tonye.wealthings.Models.WealthingsClient;
import com.tonye.wealthings.Models.WealthingsClients;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ClientsDeserializer implements JsonDeserializer<WealthingsClients> {
    @Override
    public WealthingsClients deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject object = json.getAsJsonObject();
        Map<String, String> retMap = new Gson().fromJson(object, new TypeToken<HashMap<String, String>>() {}.getType());

        WealthingsClients wealthingsClients = new WealthingsClients();
        List<WealthingsClient> clients = new ArrayList<>();

        for (Map.Entry<String, String> entry : retMap.entrySet()){
            WealthingsClient client = new WealthingsClient();

            client.setContactId(entry.getKey());
            client.setContactName(entry.getValue());
            clients.add(client);
        }

        wealthingsClients.setClients(clients);

        return wealthingsClients;
    }
}
