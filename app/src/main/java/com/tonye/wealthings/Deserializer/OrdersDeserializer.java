package com.tonye.wealthings.Deserializer;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import com.tonye.wealthings.Models.WealthingsOrders;
import com.tonye.wealthings.Models.WealthingsOrder;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OrdersDeserializer implements JsonDeserializer<WealthingsOrders> {
    @Override
    public WealthingsOrders deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject object = json.getAsJsonObject();
        Map<String, String> retMap = new Gson().fromJson(object, new TypeToken<HashMap<String, String>>() {}.getType());

        Log.d("tag", "Hello");
        WealthingsOrders wealthingsOrders = new WealthingsOrders();
        List<WealthingsOrder> orders = new ArrayList<>();

        for (Map.Entry<String, String> entry : retMap.entrySet()){
            WealthingsOrder order = new WealthingsOrder();

            order.setId(entry.getKey());
            order.setName(entry.getValue());

            Log.d("tag", "Order "+order);
            orders.add(order);
        }

        wealthingsOrders.setOrders(orders);

        return wealthingsOrders;
    }
}
