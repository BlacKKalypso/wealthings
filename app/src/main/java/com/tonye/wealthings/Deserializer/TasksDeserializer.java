package com.tonye.wealthings.Deserializer;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import com.tonye.wealthings.Models.WealthingsTask;
import com.tonye.wealthings.Models.WealthingsTasks;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TasksDeserializer implements JsonDeserializer<WealthingsTasks> {
    @Override
    public WealthingsTasks deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject object = json.getAsJsonObject();
        Map<String, String> retMap = new Gson().fromJson(object, new TypeToken<HashMap<String, String>>() {}.getType());

        WealthingsTasks wealthingsTasks = new WealthingsTasks();
        List<WealthingsTask> tasks = new ArrayList<>();

        for (Map.Entry<String, String> entry : retMap.entrySet()){
            WealthingsTask task = new WealthingsTask();

            task.setId(entry.getKey());
            task.setName(entry.getValue());
            tasks.add(task);
        }

        wealthingsTasks.setTasks(tasks);

        return wealthingsTasks;
    }
}
