package com.tonye.wealthings.Views;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tonye.wealthings.Models.TimeEntry;
import com.tonye.wealthings.R;

import java.util.List;

public class WealthingsDayEntriesAdapter extends RecyclerView.Adapter<WealthingsDayEntriesViewHolder> {
    private List<TimeEntry> timeEntries;

    public WealthingsDayEntriesAdapter(List<TimeEntry> timeEntries) {
        this.timeEntries = timeEntries;
    }

    @NonNull
    @Override
    public WealthingsDayEntriesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.fragment_timesheets_day_item, parent, false);

        return new WealthingsDayEntriesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull WealthingsDayEntriesViewHolder holder, int position) {
        holder.updateDayEntries(this.timeEntries.get(position));
    }

    @Override
    public int getItemCount() {
        Log.d("My timesheets", "TimeEntry size "+this.timeEntries.size());
        return this.timeEntries.size();
    }
}
