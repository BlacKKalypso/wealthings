package com.tonye.wealthings.Views;

import android.graphics.Typeface;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.tonye.wealthings.Models.TimeEntry;
import com.tonye.wealthings.R;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;

/*
* The objective is to model the previously created XML
* view(showing a line in the RecyclerView) into JAVA object.
* */
public class WealthingsTodayEntriesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    @BindView(R.id.home_item_title)
    TextView textViewTitle;
    @BindView(R.id.home_item_description)
    TextView textViewDescription;
    @BindView(R.id.home_item_time) TextView textViewTime;
    @BindView(R.id.play)
    Button btnPlay;
    @BindView(R.id.pause) Button btnPause;

    private WeakReference<WealthingsTodayEntriesAdapter.Listener> callbackWeakRef;

    public WealthingsTodayEntriesViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        Typeface icon = Typeface.createFromAsset(itemView.getContext().getAssets(), "icomoon.ttf");

        this.btnPlay.setVisibility(View.VISIBLE);
        this.btnPause.setVisibility(View.INVISIBLE);

        this.btnPlay.setTypeface(icon, Typeface.BOLD);
        this.btnPause.setTypeface(icon, Typeface.BOLD);

        this.btnPlay.setOnClickListener(this::onClick);
        this.btnPause.setOnClickListener(this::onClick);
    }

    public void updateTodayEntries(TimeEntry timeEntry, WealthingsTodayEntriesAdapter.Listener callback){
        this.callbackWeakRef = new WeakReference<>(callback);

        this.textViewTitle.setText(timeEntry.getClientName() + " - " + timeEntry.getOrderName());

        if (timeEntry.getTime() < 0.009) {
            this.textViewTime.setText("0.00");
        } else {
            this.textViewTime.setText(String.format("%.2f",timeEntry.getTime()));
        }

        if (timeEntry.getDescription() != null) {
            this.textViewDescription.setText(timeEntry.getDescription());
        }

        if (timeEntry.isRunning()){
            this.btnPlay.setVisibility(View.INVISIBLE);
            this.btnPause.setVisibility(View.VISIBLE);
        } else{
            this.btnPlay.setVisibility(View.VISIBLE);
            this.btnPause.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        //Play and pause time entry
        WealthingsTodayEntriesAdapter.Listener callback = callbackWeakRef.get();
        if (this.btnPlay.isShown()) {
            if (callback != null){
                callback.onClickPlayButton(getAdapterPosition());
            }
            this.btnPlay.setVisibility(View.INVISIBLE);
            this.btnPause.setVisibility(View.VISIBLE);
        } else {
            if (callback != null){
                callback.onClickPauseButton(getAdapterPosition());
            }
            this.btnPause.setVisibility(View.INVISIBLE);
            this.btnPlay.setVisibility(View.VISIBLE);
        }
    }

    public void newTimeEntry(TimeEntry timeEntry, WealthingsTodayEntriesAdapter.Listener callback){
        this.callbackWeakRef = new WeakReference<>(callback);
        if (timeEntry.getEndTime() == null) {
            this.btnPlay.setVisibility(View.INVISIBLE);
            this.btnPause.setVisibility(View.VISIBLE);
        }
    }
}
