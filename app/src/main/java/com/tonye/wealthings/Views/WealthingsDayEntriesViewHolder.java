package com.tonye.wealthings.Views;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tonye.wealthings.Models.TimeEntry;
import com.tonye.wealthings.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WealthingsDayEntriesViewHolder extends RecyclerView.ViewHolder {
    //FOR DESIGN
    @BindView(R.id.timesheets_item_tile)
    TextView textViewTitle;
    @BindView(R.id.timesheets_item_description)
    TextView textViewDescription;
    @BindView(R.id.timesheets_item_time)
    TextView textViewTime;

    public WealthingsDayEntriesViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void updateDayEntries(TimeEntry timeEntry){
        this.textViewTitle.setText(timeEntry.getClientName()+" - "+timeEntry.getOrderName());

        this.textViewTime.setText(String.format("%.2f", timeEntry.getTime()));

        if (timeEntry.getDescription() != null) {
            this.textViewDescription.setText(timeEntry.getDescription());
        }
    }
}
