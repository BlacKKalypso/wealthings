package com.tonye.wealthings.Views;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tonye.wealthings.Models.TimeEntry;
import com.tonye.wealthings.R;

import java.util.List;

public class WealthingsTodayEntriesAdapter extends RecyclerView.Adapter<WealthingsTodayEntriesViewHolder> {
    public interface Listener{
        void onClickPlayButton(int position);
        void onClickPauseButton(int position);
    }

    private final Listener callback;
    private List<TimeEntry> timeEntries;

    public WealthingsTodayEntriesAdapter(List<TimeEntry> timeEntries, Listener callback) {
        this.timeEntries = timeEntries;
        this.callback = callback;
    }

    @NonNull
    @Override
    /*
     * Create a ViewHolder from xml layout representing each line
     * of the recyclerView
     * */
    public WealthingsTodayEntriesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // CREATE VIEW HOLDER AND INFLATING ITS XML LAYOUT
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.fragment_home_today_item, parent, false);

        return new WealthingsTodayEntriesViewHolder(view);
    }
    // UPDATE VIEW HOLDER WITH A TIMESHEET
    @Override
    /*
     * Called for each of the visible lines displayed in the recyclerView
     * This is where we update their appearance
     * */
    public void onBindViewHolder(@NonNull WealthingsTodayEntriesViewHolder holder, int position) {
        holder.updateTodayEntries(this.timeEntries.get(position), this.callback);
        holder.newTimeEntry(this.timeEntries.get(position), this.callback);
    }

    @Override
    public int getItemCount() {
        return this.timeEntries.size();
    }

    public TimeEntry getEntry(int position){
        Log.d("Home", "TimeEntry size "+this.timeEntries.size());
        return this.timeEntries.get(position);
    }
}
